<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="style.css">
  <meta charset="utf-8">
  <title></title>
</head>
<body >
  <header>
  <div class="banner">
  <a href="#home"><img id="logo" src="images/logo.jpg"  alt="Логотип"/></a>

  <h1>sunny.ru</h1>
</div>
</header>

<div class="container">
  <div class="forma">
  <h2 id="form">form</h2>
  <form action="" method="POST">
    <ol>
      <?php
        if (!empty($messages)) {
        print('<div id="messages">');
        foreach ($messages as $message) {
          print($message);
        }
        print('</div>');
        }
      ?>
      <li>FIO: <input name="fio" placeholder="Введите ФИО" 
      <?php if ($errors['fio']) {print 'class="fioerror"';} ?>
       value="<?php print $values['fio']; ?>" /> 
      </li>
      <li>E-mail: <input name="email" placeholder="Введите e-mail"
      <?php if ($errors['email']) {print 'class="emailerror"';} ?> value="<?php print $values['email']; ?>" />
      </li>
      <li>Date of Birth: <input name="yob" type="date"
      <?php if ($errors['yob']) {print 'class="yoberror"';} ?> value="<?php print $values['yob']; ?>" />
      </li>
      <li>
        Gender :
        <input type="radio" checked="checked" name="gender" value="man" <?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>M
        <input type="radio" name="gender" value="woman" <?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>W
        <input type="radio" name="gender" value="another" <?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Complicated..
       
      </li>
      <li>
      Number of possibilities:
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} ?> value="1"/>1
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} ?> value="2"/>2
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} ?> value="3"/>3
        <input type="radio" checked="checked" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} ?> value="4"/>4
        <input type="radio" name="n_limbs"
        <?php if ($errors['n_limbs']) {print 'class="n_limbserror"';} ?> value="5"/>5
        
      </li>
      <li>
      Superpowers :
        <select name="sp-sp[]" multiple="multiple" <?php if ($errors['sp-sp']) {print 'class="sp-sperror"';} ?>>
          <option value="immortality">Immortality</option>
          <option value="passing_through_walls">Levitation</option>
          <option value="levitation">Elemental Magic</option>
          <option value="wonder">Eternal happiness</option>
        </select>
        
      </li>
      <li>
        BIO :<textarea name="bio"  
        <?php if ($errors['bio']) {print 'class="bioerror"';} ?> 
        placeholder="Введите свою биографию" ><?php print $values['bio']; ?></textarea>
        
      </li>
      <li>
        <input type="checkbox" name="Galochka" 
        <?php if ($errors['Galochka']) {print 'class="Galochkaerror"';} ?> />Familiarized  
      </li>
      <li>
        <input type="submit" value="Отправить" />
      </li>
    </ol>
  </form>
</div>
</div>
<footer>
<h2>Believe in yourself.</h2>
</footer>
</body>
</html>
