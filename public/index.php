<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['yob'] = !empty($_COOKIE['yob_error']);
    $errors['n_limbs'] = !empty($_COOKIE['n_limbs_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['sp-sp'] = !empty($_COOKIE['sp-sp_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['Galochka'] = !empty($_COOKIE['Galochka_error']);
    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="fioerror">  Заполните имя.</div>';

    }
    

    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="emailerror">  Заполните почту.</div>';
    }
    if ($errors['yob']) {
        setcookie('yob_error', '', 100000);
        $messages[] = '<div class="yoberror">  Заполните дату.</div>';
    }
    if ($errors['n_limbs']) {
        setcookie('n_limbs_error', '', 100000);
        $messages[] = '<div class="n_limbserror">  Выберите количество конечностей.</div>';
    }
    if ($errors['gender']) {
        setcookie('gender_error', '', 100000);
        $messages[] = '<div class="gendererror">  Заполните пол.</div>';
    }
    if ($errors['sp-sp']) {
        setcookie('sp-sp_error', '', 100000);
        $messages[] = '<div class="sp-sperror">  Выберите сверхспособность.</div>';
    }
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="bioerror">  Заполните биографию.</div>';
    }
    if ($errors['Galochka']) {
        $messages[] = '<div class="Galochkaerror">  Поставьте галочку.</div>';
    }
    if (!$errors['fio'] && !$errors['email'] && !$errors['yob'] && !$errors['n_limbs'] && !$errors['gender'] && !$errors['sp-sp'] && !$errors['bio'] && !$errors['Galochka']  ) {
        if (!empty($_COOKIE['save'])) {
            setcookie('save', '', 100000);
            $messages[] = 'Спасибо, результаты сохранены.';
          }
    }
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['yob'] = empty($_COOKIE['yob_value']) ? '' : $_COOKIE['yob_value'];
    $values['n_limbs'] = empty($_COOKIE['n_limbs_value']) ? '' : $_COOKIE['n_limbs_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['sp-sp'] = empty($_COOKIE['sp-sp_value']) ? '' : $_COOKIE['sp-sp_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['Galochka'] = empty($_COOKIE['Galochka_value']) ? '' : $_COOKIE['Galochka_value'];
    include('form.php');
    exit();
}
 
else{
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['yob'])) {
        setcookie('yob_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('yob_value', $_POST['yob'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['n_limbs'])) {
        setcookie('n_limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('n_limbs_value', $_POST['n_limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['gender'])) {
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['sp-sp'])) {
        setcookie('sp-sp_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('sp-sp_value', $_POST['sp-sp'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['Galochka'])) {
        setcookie('Galochka_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('Galochka_value', $_POST['Galochka'], time() + 30 * 24 * 60 * 60);
    }
    if ($errors) {
        header('Location: index.php');
        exit();
      }
      else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('yob_error', '', 100000);
        setcookie('n_limbs_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('sp-sp_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('Galochka_error', '', 100000);
    }
   
    $conn = new PDO("mysql:host=localhost;dbname=u24052", 'u24052', '4700864', array(PDO::ATTR_PERSISTENT => true));

    $user = $conn->prepare("INSERT INTO users SET fio = ?, email = ?, yob = ?, gender = ?, n_limbs = ?, bio = ?");
    if(isset($_POST['fio'])){
        $user -> execute([$_POST['fio'], $_POST['email'], $_POST['yob'], $_POST['gender'], $_POST['n_limbs'], $_POST['bio']]);
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
    }
    $id_user = $conn->lastInsertId();

    $abilitys = $conn->prepare("INSERT INTO abilitys SET id_user = ?");
    $abilitys -> execute([$id_user]);
    $id_abil = $conn->lastInsertId();
    if(isset($_POST['sp-sp'])){
        $abil = implode(',',$_POST['sp-sp']);
        $ability = $conn->prepare("INSERT INTO ability SET abilitysId = ?, names_ability = ?");
        $ability -> execute([$id_abil, $abil]);
    }
    setcookie('save', '1');
    header('Location: index.php');
}
?>
